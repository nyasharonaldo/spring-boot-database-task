DROP DATABASE IF EXISTS stocks;

CREATE DATABASE stocks;

use stocks;

DROP TABLE IF EXISTS stock;

CREATE TABLE stock(
	id INT NOT NULL,
    ticker VARCHAR(4) NOT NULL,
    companyName VARCHAR(40) NOT NULL
);

INSERT INTO stock VALUES(1,'AMZN','Amazon');

INSERT INTO stock VALUES(2,'AAPL','Apple');


INSERT INTO stock VALUES(3,'TSLA','Tesal');