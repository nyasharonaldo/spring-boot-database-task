package com.example.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.stock.*;

public interface StockDao extends CrudRepository<Stock, Long>{
	

}
