package com.example.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.repo.*;
import com.example.stock.*;

@RestController
@RequestMapping("/stocks")
public class StockController {
	
	private static final Logger LOG =
			LoggerFactory.getLogger(StockController.class);
	
	@Autowired
	StockDao stockDao;
	
	@RequestMapping(method=RequestMethod.GET)
    public Iterable<Stock> findAll(){
    	LOG.info("HTTP Get to findAll()");
    	LOG.debug("This is much more verbose debugging message");
        return stockDao.findAll();
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public Stock create(@RequestBody Stock stock) {
    	stockDao.save(stock);
    	return stock;
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.GET)
    public Stock findById(@PathVariable long id) {
    	return stockDao.findById(id).get();
    }
    
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
    public void deleteById(@PathVariable long id) {
    	stockDao.deleteById(id);
    }
    
    
    
	
	
	
	
	
	
	

}
